﻿using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Spartez.TFS4JIRA.CheckInPolicy.JIRA;

namespace Spartez.TFS4JIRA.CheckInPolicy.Settings
{
    public sealed class LocalCheckInPolicySettingsStorage : ApplicationSettingsBase
    {
        [UserScopedSetting]
        public List<LocalCheckInPolicySettings> LocalCheckInPolicySettings
        {
            get
            {
                if (this["LocalCheckInPolicySettings"] == null)
                {
                    this["LocalCheckInPolicySettings"] = new List<LocalCheckInPolicySettings>();
                }

                return (List<LocalCheckInPolicySettings>)this["LocalCheckInPolicySettings"];
            }
            set { this["LocalCheckInPolicySettings"] = value; }
        }

        public bool HasSettingsForJiraUrl(string jiraUrl)
        {
            return GetSettingsForJiraUrl(jiraUrl) != null;
        }

        public LocalCheckInPolicySettings GetSettingsForJiraUrl(string jiraUrl)
        {
            return LocalCheckInPolicySettings.FirstOrDefault(x => x.JiraUrl != null && jiraUrl == x.JiraUrl);
        }

        public void AddSettings(LocalCheckInPolicySettings localCheckInPolicySettings)
        {
            LocalCheckInPolicySettings.RemoveAll(x => x.JiraUrl == localCheckInPolicySettings.JiraUrl);
            LocalCheckInPolicySettings.Add(localCheckInPolicySettings);
        }
    }
}
